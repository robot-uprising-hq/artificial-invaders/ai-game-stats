## Artificial Invaders - Game Stats Server

This server will be running during the competition in the same LAN as the video stream. The referees will update stats manually. So, goals will be registered after a some delay, ~ 1-3 seconds. 

The game situation can be fetched from *url/stats*. The data format will be JSON.

The robots are allowed to move / play only when **running** is true. 

- A round starts officially only when *running* becomes *true*.
- A round ends officially only when *running* becomes *false*.

The variable **time** tells how many seconds has passed. The variable *running* becomes *false* when *time* >= *round_duration*. 




```JSON
{
    "running": false,  // is the game running
    "time": 0.0,  // how many seconds has passed
    "round_duration": 120,  // how many seconds a round lasts
    "red": {
        "score": 0.0,  // red's score
        "bomb": 0,  // the number of bomb balls in the red goal
        "energy": 0  // the number of energy balls in the red goal
    },
    "blue": {
        "score": 0.0,
        "bomb": 0,
        "energy": 0
    }
}
```

In the *server.py*, there is example urls that show how you can test the server on your own.

Note: It's not allowed to hack or bruteforce our servers nor the particpants' servers.