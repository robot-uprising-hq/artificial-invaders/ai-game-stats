import React from "react";
import "./TopBar.css";

const TopBar = ({ team, onChangeTeam }) => (
  <div className="TopBar">
    <button
      onClick={() => onChangeTeam("blue")}
      className="TeamButton Blue"
      style={{ flex: team === "blue" ? "3 1 auto" : "1 1 auto" }}
    >
      Blue
    </button>
    <button
      onClick={() => onChangeTeam("red")}
      className="TeamButton Red"
      style={{ flex: team === "red" ? "3 1 auto" : "1 1 auto" }}
    >
      Red
    </button>
  </div>
);

export default TopBar;
