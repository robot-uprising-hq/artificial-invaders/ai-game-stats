import React from "react";
import "./ExtraButtons.css";

class ExtraButtons extends React.Component {
  state = { show: true };

  onToggleShow = () => {
    this.setState({ show: !this.state.show });
  };

  render() {
    const { show } = this.state;
    const { adminRequest } = this.props;
    return (
      <div className="Bottom">
        <button className="ShowHideButton" onClick={this.onToggleShow}>
          {show ? "Hide" : "Show more"}
        </button>
        {show && (
          <div className="ExtraButtons">
            <button
              className="BasicButton"
              onClick={() => adminRequest("/reset")}
            >
              Reset
            </button>
            <button
              className="BasicButton"
              onClick={() => adminRequest("/start")}
            >
              Start Game
            </button>
            <button
              className="BasicButton"
              onClick={() => adminRequest("/end")}
            >
              End Game
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default ExtraButtons;
