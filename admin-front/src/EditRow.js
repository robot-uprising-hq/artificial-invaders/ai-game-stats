import React from "react";
import "./EditRow.css";

const EditRow = ({ label, value, onDecrease, onIncrease }) => (
  <div className="EditRow">
    <button className="BasicButton" onClick={onDecrease}>
      -
    </button>
    <div className="Label">{label}: {value}</div>
    <button className="BasicButton" onClick={onIncrease}>
      +
    </button>
  </div>
);

export default EditRow;
