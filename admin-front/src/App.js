import React from "react";
import EditRow from "./EditRow";
import TopBar from "./TopBar";
import Timer from "./Timer";
import "./App.css";
import ExtraButtons from "./ExtaButtons";

class App extends React.Component {
  state = {
    team: "blue",
    key: "secret",
    backendUrl: "http://127.0.0.1:5000",
    stats: {
      blue: {},
      red: {}
    }
  };

  // Start fetching
  componentDidMount() {
    setInterval(this.fetchStats, 500);
  }

  // Request action from admin's GET API
  adminRequest = async path => {
    const { backendUrl, key } = this.state;
    try {
      const resp = await fetch(`${backendUrl}/admin${path}?key=${key}`);
      const data = await resp.json();
      this.fetchStats(); // start fetching data immediately
      return data;
    } catch (err) {
      console.warn(err);
    }
  };

  // Fetche statistics
  fetchStats = async () => {
    const { backendUrl } = this.state;
    try {
      const resp = await fetch(backendUrl + "/stats");
      const stats = await resp.json();
      this.setState({ stats });
    } catch (err) {
      console.warn(err);
    }
  };

  // Change to modify different team
  changeTeam = team => {
    this.setState({ team });
  };

  render() {
    const { team, stats } = this.state;
    const { time, round_duration } = stats;
    const { energy, bomb } = stats[team];
    const timeLeft = Math.max(round_duration - time, 0);
    return (
      <div className="App">
        <TopBar team={team} onChangeTeam={this.changeTeam} />
        <div className="Content">
          <Timer timeLeft={timeLeft} />
          <EditRow
            onDecrease={() => this.adminRequest(`/add/${team}/energy/-1`)}
            onIncrease={() => this.adminRequest(`/add/${team}/energy/1`)}
            label="Energy"
            value={energy}
          ></EditRow>
          <EditRow
            onDecrease={() => this.adminRequest(`/add/${team}/bomb/-1`)}
            onIncrease={() => this.adminRequest(`/add/${team}/bomb/1`)}
            label="Bomb"
            value={bomb}
          ></EditRow>
          <ExtraButtons adminRequest={this.adminRequest} />
        </div>
      </div>
    );
  }
}

export default App;
