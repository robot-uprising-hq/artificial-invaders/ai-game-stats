import React from "react";
import "./Timer.css";

const Timer = ({ timeLeft }) => (
  <div className="Timer">
    {Math.floor(timeLeft / 60)}:{timeLeft % 60 < 10 ? 0 : null}
    {Math.floor(timeLeft % 60)}
  </div>
);

export default Timer;
