import time
import copy
import sys
from flask import Flask, json, request, abort
from flask_cors import CORS

# > pip install flask

api = Flask(__name__)
CORS(api)

default_stats = {
    "running": False,  # is the game running
    "time": 0.0,  # how many seconds has passed
    "round_duration": 120,  # how many seconds a round lasts
    "red": {
        "score": 0.0,  # red's score
        "bomb": 0,  # the number of bomb balls in the red goal
        "energy": 0  # the number of energy balls in the red goal
    },
    "blue": {
        "score": 0.0,
        "bomb": 0,
        "energy": 0
    }
}

stats = copy.deepcopy(default_stats)
start_time = time.time()

##########################
### Participant Routes ###
##########################


# Returns stats as JSON
# http://127.0.0.1:5000/stats
@api.route('/stats')
def get_stats():
    global stats

    if stats["running"]:
        stats["time"] = time.time() - start_time
        if stats["time"] >= stats["round_duration"]:
            stats["running"] = False

    return json.dumps(stats)

##########################
###### Admin Routes ######
##########################

# Note: The server is made to be easily testable
# without UI. That's why we are using Get instead of Post.


# Note: It's not allowed to hack our server.
# The idea behind secret_key is to stop accidental admin request.
secret_key = "secret"
if len(sys.argv) > 1:
    secret_key = sys.argv[1]


def authenticate():
    if secret_key != request.args.get("key"):
        abort(403)


success_msg = json.dumps({"Success": True})

# http://127.0.0.1:5000/admin/start?key=secret
@api.route('/admin/start')
def start_game():
    global start_time
    authenticate()

    if stats["running"] == True:
        return json.dumps({"Success": False, "Error": "Game already running"})
    start_time = time.time()
    stats["running"] = True
    return success_msg

# http://127.0.0.1:5000/admin/start?key=secret
@api.route('/admin/end')
def end_game():
    global start_time
    authenticate()
    stats["running"] = False
    return success_msg

# http://127.0.0.1:5000/admin/reset?key=secret
@api.route('/admin/reset')
def reset_game():
    global stats
    authenticate()
    stats = copy.deepcopy(default_stats)
    return success_msg

# http://127.0.0.1:5000/admin/reset?key=secret
@api.route('/admin/restart')
def restart_game():
    reset_game()
    return start_game()

# http://127.0.0.1:5000/admin/add/blue/bomb?key=secret
@api.route('/admin/add/<team>/bomb')
@api.route('/admin/add/<team>/bomb/<amount>')
def add_bomb(team, amount=1):
    authenticate()
    stats[team]["bomb"] += int(amount)
    stats[team]["score"] -= 1.0*int(amount)

    return success_msg

# http://127.0.0.1:5000/admin/add/red/energy?key=secret
@api.route('/admin/add/<team>/energy')
@api.route('/admin/add/<team>/energy/<amount>')
def add_energy(team, amount=1):
    authenticate()
    stats[team]["energy"] += int(amount)
    stats[team]["score"] += 1.0*int(amount)
    return success_msg


if __name__ == '__main__':
    print("#"*25)
    print("SECRET_KEY:", secret_key)
    print("#"*25)
    api.run()
